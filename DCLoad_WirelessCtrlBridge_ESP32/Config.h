/*
 * Config.h
 *
 *  Created on: 25.03.2018
 *      Author: Witold
 */

#ifndef CONFIG_H_
#define CONFIG_H_

namespace CONFIG {

static const uint32_t SERIAL_CONSOLE_BAUD = 115200;

static const uint32_t SERIAL_DCLOAD_UART = 2;       // UART2 = PIN 16, 17
static const uint32_t SERIAL_DCLOAD_BAUD = 9600;

} /* Namespace CONFIG */

#endif /* CONFIG_H_ */
