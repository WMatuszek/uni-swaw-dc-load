/*
 * UtilFunc.h
 *
 *  Created on: 25.03.2018
 *      Author: Witold
 */

#ifndef UTILFUNC_H_
#define UTILFUNC_H_

#include <Arduino.h>

static inline void BlueLED_INIT() {
    pinMode(LED_BUILTIN, OUTPUT);
}

static inline void BlueLED_ON() {
    digitalWrite(LED_BUILTIN, HIGH);
}
static inline void BlueLED_OFF() {
    digitalWrite(LED_BUILTIN, LOW);
}

#endif /* UTILFUNC_H_ */
