#include "Arduino.h"

#if defined(ESP8266)
#include <ESP8266WiFi.h>
#else
#include <WiFi.h>
#endif

//needed for library
#include <DNSServer.h>
#if defined(ESP8266)
#include <ESP8266WebServer.h>
#else
#include <WebServer.h>
#endif
#include <WiFiManager.h>

#include <TelnetBridge.h>

#include "Config.h"
#include "UtilFunc.h"

/**
 * Telnet bridge, connects LAN to device connected to ESP32 via
 *
 */
TelnetBridge telnetServ(CONFIG::SERIAL_DCLOAD_UART, CONFIG::SERIAL_DCLOAD_BAUD);

/**
 * WIFI manager callback for AP creation
 * @param myWiFiManager
 */
void configModeCallback(WiFiManager *myWiFiManager) {
    Serial.println("Entered config mode");
    Serial.println(WiFi.softAPIP());
    //if you used auto generated SSID, print it
    Serial.println(myWiFiManager->getConfigPortalSSID());
}


void setup() {
    Serial.begin(CONFIG::SERIAL_CONSOLE_BAUD);
    BlueLED_INIT();

    WiFiManager wifiManager;
    wifiManager.setConnectTimeout(10);
    wifiManager.setAPCallback(configModeCallback);

    if (false == wifiManager.autoConnect("ESP32_DCLOAD_BRIDGE", "dupadupa")) {
        Serial.println("failed to connect and hit timeout");

        ESP.restart();
        delay(1000);
    }

    Serial.println("--- WIFI MANAGER DONE");

    telnetServ.initialize();
}

void loop() {
    BlueLED_ON();
    telnetServ.loop();
    BlueLED_OFF();
    delay(1000);
}
