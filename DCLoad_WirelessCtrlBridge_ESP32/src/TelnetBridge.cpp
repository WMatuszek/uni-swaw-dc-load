/*
 * TelnetServer.cpp
 *
 *  Created on: 24.03.2018
 *      Author: Witold
 */

#include <TelnetBridge.h>

TelnetBridge::TelnetBridge(uint32_t deviceUARTnum, uint32_t baudRate) {
    serialToDevice = HardwareSerial(deviceUARTnum);
    serialToDeviceBaudRate = baudRate;
    server = WiFiServer(TELNET_PORT);
}

bool TelnetBridge::initialize() {
    if (WiFi.isConnected() == false) {
        Serial.print("No WIFI connection, telnet bridge init fail");
        return false;
    }
    // Start serial to device and the server
    serialToDevice.begin(serialToDeviceBaudRate);
    server.begin();
    server.setNoDelay(true);

    Serial.print("Ready! Use 'telnet ");
    Serial.print(WiFi.localIP());
    Serial.println(" 23' to connect");
    return true;
}

void TelnetBridge::loop() {
    uint8_t i;

    // Check WiFI connection
    if (WiFi.isConnected() == false) {
        Serial.println("WiFi not connected!");
        for (i = 0; i < MAX_SRV_CLIENTS; i++) {
            if (serverClients[i])
                serverClients[i].stop();
        }
        return;
    }

    // Check for new telnet server clients
    if (server.hasClient()) {
        for (i = 0; i < MAX_SRV_CLIENTS; i++) {
            // Find free/disconnected spot
            if (!serverClients[i] || !serverClients[i].connected()) {
                if (serverClients[i])
                    serverClients[i].stop();
                serverClients[i] = server.available();
                if (!serverClients[i])
                    Serial.println("available broken");
                Serial.print("New client: ");
                Serial.print(i);
                Serial.print(' ');
                Serial.println(serverClients[i].remoteIP());
                break;
            }
        }
        if (i >= MAX_SRV_CLIENTS) {
            // No free/disconnected spot so reject
            server.available().stop();
        }
    }

    // Check server clients for data to device
    for (i = 0; i < MAX_SRV_CLIENTS; i++) {
        if (serverClients[i] && serverClients[i].connected()) {
            if (serverClients[i].available()) {
                // Get data from the telnet client and push it to the UART
                while (serverClients[i].available())
                    serialToDevice.write(serverClients[i].read());
            }
        } else {
            if (serverClients[i]) {
                serverClients[i].stop();
            }
        }
    }

    // Check device serial for data to clients
    if (serialToDevice.available()) {
        size_t len = serialToDevice.available();
        uint8_t sbuf[len];
        serialToDevice.readBytes(sbuf, len);
        // Push UART data to all connected telnet clients
        for (i = 0; i < MAX_SRV_CLIENTS; i++) {
            if (serverClients[i] && serverClients[i].connected()) {
                serverClients[i].write(sbuf, len);
                delay(1);
            }
        }
    }
}

