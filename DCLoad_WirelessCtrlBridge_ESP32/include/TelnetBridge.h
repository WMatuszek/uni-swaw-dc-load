/*
 * TelnetServer.h
 *
 *  Created on: 24.03.2018
 *      Author: Witold
 */

#ifndef TELNETBRIDGE_H_
#define TELNETBRIDGE_H_

#include <stdint.h>

#include "WiFi.h"
#include <WiFiServer.h>
#include <WiFiClient.h>
#include <HardwareSerial.h>

/**
 * Telnet bridge class
 * Connects LAN clients to some device connected to ESP32 via UART
 */
class TelnetBridge {

public:
    static constexpr uint16_t MAX_SRV_CLIENTS = 1;

    static const uint32_t DEFAULT_DEVICE_UART = 2;

    static const uint16_t TELNET_PORT = 23;

protected:
    WiFiServer server;
    WiFiClient serverClients[MAX_SRV_CLIENTS];

    HardwareSerial serialToDevice = HardwareSerial(DEFAULT_DEVICE_UART);

    uint32_t serialToDeviceBaudRate;

public:
    TelnetBridge(uint32_t deviceUARTnum, uint32_t baudRate = 9600);
    bool initialize();
    void loop();
};


#endif /* TELNETBRIDGE_H_ */
